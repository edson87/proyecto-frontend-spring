import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { environment } from 'src/enviroment/environment';


@Injectable({
  providedIn: 'root'
})
export class PersonaJuridicaService {
  private api_url = environment.API_URL;
  private base = 'persona_natural'; 

  private http = inject(HttpClient);

  constructor() { }


  public create(body:Object) {
    const headers= new HttpHeaders()
      .set('content-type', 'application/json')
      .set('Access-Control-Allow-Origin', '*');
    const route = `${this.api_url}/${this.base}`;
    return this.http.post(route,body,{headers: headers});
  }

  public getAll(page:number, size:number){
    const route = `${this.api_url}/${this.base}?page=${page}&size=${size}`;
    return this.http.get(route);
  }

  public getbyId(id:number){
    const route = `${this.api_url}/${this.base}/${id}`;
    return this.http.get(route);
  }

  public search(text:any){
    const route = `${this.api_url}/${this.base}/get_by_name?nombre=${text}`;
    return this.http.get(route);
  }

  public delete(id:number){
    const route = `${this.api_url}/${this.base}/${id}`;
    return this.http.delete(route);
  }

  public update(body:Object, id:number){
    const route = `${this.api_url}/${this.base}/${id}`;
    return this.http.put(route,body);
  }
}


