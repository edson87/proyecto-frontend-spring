import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarPersonaJuridicaComponent } from './listar-persona-juridica.component';

describe('ListarPersonaJuridicaComponent', () => {
  let component: ListarPersonaJuridicaComponent;
  let fixture: ComponentFixture<ListarPersonaJuridicaComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ListarPersonaJuridicaComponent]
    });
    fixture = TestBed.createComponent(ListarPersonaJuridicaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
