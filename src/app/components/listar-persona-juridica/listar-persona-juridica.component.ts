import { Component, OnInit, inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PersonaJuridicaService } from 'src/app/services/persona-juridica.service';

@Component({
  selector: 'app-listar-persona-juridica',
  templateUrl: './listar-persona-juridica.component.html',
  styleUrls: ['./listar-persona-juridica.component.scss']
})
export class ListarPersonaJuridicaComponent implements OnInit {
  personas = [];
  paginador:any;
  searchText:any = '';
  sizeSelected:number = 5;

  private personasJuridicoService = inject(PersonaJuridicaService);
  private activatedRoute =  inject(ActivatedRoute);

  constructor(){

  }

  ngOnInit(): void {
    let page = Number(this.activatedRoute.snapshot.paramMap.get('page'));
    let size = this.sizeSelected;
    if(!page){ 
      page = 0
    }
    /* this.activatedRoute.paramMap.subscribe(params => {
      let page: number = +params.get('page');

      if (!page) {
        page = 0;
      } */
    this.getPersonal(page,size)
  }

  getPersonal(page:number, size:number){
    this.paginador = []
    this.personasJuridicoService.getAll(page,size)
    .subscribe({
      next:(resp:any)=>{
        this.personas = resp.content
        this.paginador = resp;
      }
    })
  }

  search(){
    console.log(this.searchText);
    this.personasJuridicoService.search(this.searchText)
      .subscribe({
        next: (resp:any) => {
          this.personas = resp;
          this.paginador = resp;
        }
      })
  }

  keyupChange(){
    setTimeout(() => {
      if(this.searchText == ''){
        this.getPersonal(0,10);
      }
    }, 1000);
  }

  expantList(){
    console.log(this.sizeSelected);
    this.getPersonal(0, this.sizeSelected);
  }

  delete(id:number){
    this.personasJuridicoService.delete(id)
     .subscribe({
      next: resp => {
        this.getPersonal(0,this.sizeSelected)
      }
     })
  }
}
