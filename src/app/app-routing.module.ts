import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AgregarPersonaJuridicaComponent } from './components/agregar-persona-juridica/agregar-persona-juridica.component';
import { ListarPersonaJuridicaComponent } from './components/listar-persona-juridica/listar-persona-juridica.component';

const routes: Routes = [
  { path: '', redirectTo: '/listarPJ', pathMatch: 'full' },
  {
    path:'agregarPJ',
    component: AgregarPersonaJuridicaComponent
  },
  {
    path:'agregarPJ/:id',
    component: AgregarPersonaJuridicaComponent
  },
  {
    path:'listarPJ',
    component: ListarPersonaJuridicaComponent
  },
  {
    path:'listarPJ/page/:page',
    component: ListarPersonaJuridicaComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
