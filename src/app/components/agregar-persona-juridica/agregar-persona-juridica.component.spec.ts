import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AgregarPersonaJuridicaComponent } from './agregar-persona-juridica.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('AgregarPersonaJuridicaComponent', () => {
  let component: AgregarPersonaJuridicaComponent;
  let fixture: ComponentFixture<AgregarPersonaJuridicaComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AgregarPersonaJuridicaComponent],
      imports: [HttpClientTestingModule],
    });
    fixture = TestBed.createComponent(AgregarPersonaJuridicaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create register', () => {
    expect(component).toBeTruthy();
  });
});
