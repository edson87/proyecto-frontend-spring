import { Component, OnInit, inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { PersonaJuridicaService } from 'src/app/services/persona-juridica.service';

@Component({
  selector: 'app-agregar-persona-juridica',
  templateUrl: './agregar-persona-juridica.component.html',
  styleUrls: ['./agregar-persona-juridica.component.scss']
})
export class AgregarPersonaJuridicaComponent implements OnInit {
  form: FormGroup = new FormGroup({});
  id:number = 0;
  forSave:boolean = false

  private personaJuridicaService = inject(PersonaJuridicaService);
  private route = inject(ActivatedRoute);
  private router = inject(Router);

  constructor(private fb: FormBuilder){
    this.id = this.route.snapshot.paramMap.get('id') != null? Number(this.route.snapshot.paramMap.get('id')) : 0;
    if(this.id != null && this.id != 0){
      this.forSave = true
      this.getPersonaJuridica(this.id);
    }
    this.initForm();
  }


  ngOnInit(): void {
  }

  initForm(){
    this.form = this.fb.group({
      id: [null],
      nombres: [null, Validators.required],
      primer_apellido: [null, Validators.required],
      segundo_apellido: [null, Validators.required],
      nro_documento_identidad: [null, Validators.required],
      celular: [null, Validators.required],
      direccion_domicilio: [null, Validators.required],
    })
  }

  getPersonaJuridica(id:number){
    this.personaJuridicaService.getbyId(id)
      .subscribe({
        next: resp => {
          this.form.patchValue(resp);
        }
      })
  }

  save(){
    console.log(this.form.value);
    const form = this.form.getRawValue();

    this.personaJuridicaService.create(form).subscribe({
      next: resp => {
          console.log(resp);         
      }
    })
  }

  update() {
    const form = this.form.getRawValue();
    this.personaJuridicaService.update(form, form.id).subscribe({
      next: resp => {
          console.log(resp); 
          this.router.navigate(['/listarPJ'])        
      }
    })
  }

  back(){
    this.router.navigate(['/listarPJ'])
  }
}
