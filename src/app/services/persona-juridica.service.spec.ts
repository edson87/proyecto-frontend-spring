import { TestBed } from '@angular/core/testing';

import { PersonaJuridicaService } from './persona-juridica.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClientModule } from '@angular/common/http';

describe('PersonaJuridicaService', () => {
  let service: PersonaJuridicaService;
  let httpTestingController: HttpTestingController;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers:[PersonaJuridicaService]
    });
    
   //httpTestingController = TestBed.get(HttpTestingController);
   //service = TestBed.inject(PersonaJuridicaService);
   
  });

  it('should be created', () => {
    const service = TestBed.get(PersonaJuridicaService)
      expect(service).toBeTruthy();
  });

  it('should be created2', () => {
    service = TestBed.inject(PersonaJuridicaService);
    const ele:any = {
      "nombres": "Carmen ",
      "primer_apellido": "Flores",
      "segundo_apellido":"Mendieta",
      "nro_documento_identidad": "5280036",
      "celular":"70396587",
      "direccion_domicilio":"Calle Bolivar"
  }
    service.create(ele).subscribe({
      next:resp=>{
        expect(service).toBeTruthy();

      }
    })
  });

  it('should get by id', () => {
    service = TestBed.inject(PersonaJuridicaService);
    const ele:any = {
      "nombres": "Carmen ",
      "primer_apellido": "Flores",
      "segundo_apellido":"Mendieta",
      "nro_documento_identidad": "5280036",
      "celular":"70396587",
      "direccion_domicilio":"Calle Bolivar"
  }
    service.getbyId(1).subscribe({
      next:resp=>{
        expect(resp).toBeTruthy();

      }
    })
  });

  it('should get by filter', () => {
    service = TestBed.inject(PersonaJuridicaService);
    const ele:any = {
      "nombres": "Carmen ",
      "primer_apellido": "Flores",
      "segundo_apellido":"Mendieta",
      "nro_documento_identidad": "5280036",
      "celular":"70396587",
      "direccion_domicilio":"Calle Bolivar"
  }
    service.search('carme').subscribe({
      next:resp=>{
        expect(resp).toEqual(ele)
      }
    })
  });

  it('#getData should return expected data', (done) => {
    service = TestBed.inject(PersonaJuridicaService);
  
    service.getAll(0,10).subscribe(data => {
      console.log(data);
      
      expect(data);
      done();
    });

    //const testRequest = httpTestingController.expectOne('http://localhost:8080');

    //testRequest.flush(expectedData);
  });
});
